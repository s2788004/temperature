package nl.utwente.di.temperature;

public class TempConverter {

    double convertToFahrenheit(String celsius) {
        return Double.parseDouble(celsius) * 1.8 + 32;
    }
}
